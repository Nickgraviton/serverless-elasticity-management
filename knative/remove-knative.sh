kubectl delete --filename https://github.com/knative/net-kourier/releases/download/v0.20.0/kourier.yaml
kubectl delete --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-hpa.yaml
kubectl delete --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-core.yaml
kubectl delete --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-crds.yaml
kubectl patch crd/ingresses.networking.internal.knative.dev -p '{"metadata":{"finalizers":[]}}' --type=merge
kubectl patch crd/routes.serving.knative.dev -p '{"metadata":{"finalizers":[]}}' --type=merge
kubectl api-resources -o name | grep knative | xargs kubectl delete crd
