# Setup Knative
In order to setup Knative to work with the Nginx ingress controller, we can change Kourier to a ClusterIP service and remove its Nodeports. To override the default values we can create a yaml file like:
```yaml
spec:
  ports:
  - name: http2
    port: 80
    protocol: TCP
    targetPort: 8080
  - name: https
    port: 443
    protocol: TCP
    targetPort: 8443
  type: ClusterIP
```
with which we can patch the kourier service by running:
```bash
kubectl patch svc/kourier \
  -n kourier-system \
  --type merge \
  -p "$(cat override.yaml)"
```
