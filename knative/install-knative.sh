kubectl apply --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-crds.yaml
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-core.yaml

kubectl apply --filename https://github.com/knative/serving/releases/download/v0.20.0/serving-hpa.yaml

kubectl apply --filename https://github.com/knative/net-kourier/releases/download/v0.20.0/kourier.yaml
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'

kubectl patch configmap/config-domain \
  -n knative-serving \
  --type merge \
  -p '{"data":{"127.0.0.1.nip.io":""}}'

kubectl patch svc/kourier \
  -n kourier-system \
  --type merge \
  -p "$(cat override.yaml)"
