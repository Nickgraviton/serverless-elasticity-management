# A Kubernetes environment to setup and test serverless applications
More information can be found in the repo's wiki pages. The wiki pages' repo can be cloned by running:

`git clone https://gitlab.com/Nickgraviton/serverless-elasticity-management.wiki.git`

## Repository structure
```
├── docs
│   └── metrics.md
├── examples
│   ├── inputs
│   ├── k8s
│   ├── knative
│   └── kubeless
├── k8s-cluster
│   ├── create-cluster.sh
│   ├── delete-cluster-master.sh
│   └── delete-cluster-worker.sh
├── knative
│   ├── after-reboot.sh
│   ├── install-knative.sh
│   ├── override.yaml
│   ├── port-forward-kourier.sh
│   ├── README.md
│   └── remove-knative.sh
├── kubeless
│   ├── install-kubeless.sh
│   └── remove-kubeless.sh
├── metrics-server
│   ├── components.yaml
│   └── README.md
├── nginx-ingress
│   ├── helm-install.sh
│   ├── helm-uninstall.sh
│   ├── override.yaml
│   └── port-forward-ingress.sh
├── prometheus
│   ├── grafana-credentials.txt
│   ├── helm-install.sh
│   ├── helm-uninstall.sh
│   ├── port-forward-grafana.sh
│   └── port-forward-prometheus.sh
├── prometheus-adapter
│   ├── helm-install.sh
│   ├── helm-uninstall.sh
│   ├── override.yaml
│   └── README.md
├── README.md
└── src
    └── metrics.py
```
### docs:
- `metrics.md:` List of metrics to be collected by a script
### examples:
- `inputs:` Directory with example inputs
- `k8s:` Example vanilla Kubernetes deployments
- `knative:` Example Knative services
- `kubeless:` Example Kubeless services
### k8s-cluster:
- `create-cluster.sh:` Bash script to initalize cluster in the master node
- `delete-cluster-master.sh:` Bash script to cleanup in the master node
- `delete-cluster-worker.sh:` Bash script to cleanup in each worker node
### knative:
- `after-reboot.sh:` Bash script to be run after rebooting if some pods don't become ready
- `install-knative.sh:` Bash script to install Knative
- `override.yaml:` Yalm of overrides of default values for the Kourier service to change it to a ClusterIP service
- `port-forward-kourier.sh:` Bash script to port forward port 8080 to Kourier's port 8080 so that we can send requests to Knative through localhost:8080
- `README.md:` Markdown file explaining how to setup Kourier
- `remove-knative.sh:` Bash script to remove Knative's components from the cluster
### kubeless:
- `install-kubeless.sh:` Bash script to install Kubeless
- `remove-kubeless.sh:` Bash script to remove Kubeless' components from the cluster
### metrics-server:
- `components.yaml:` Fixed yaml deployment file for the metrics server
- `README.md:` Markdown file explaining how to install the metrics server
### nginx-ingress
- `helm-install.sh:` Bash script that installs the nginx ingress using helm
- `helm-uninstall.sh:` Bash script that removes the nginx ingress from the cluster
- `override.yaml:` Yaml of overrides of default values for the nginx ingress to allow it to be scraped by prometheus
- `port-forward-ingress.sh:` Bash script to port forward port 8000 to the ingress' port 80 so that we can access the ingress through localhost:8000
### prometheus:
- `grafana-credentials.txt:` The default user's credentials for the Grafana dashboard login
- `helm-install.sh:` Bash script that installs the Prometheus stack using helm
- `helm-uninstall.sh:` Bash script that removes the Prometheus stack from the cluster
- `port-forward-grafana.sh:` Bash script to port forward port 3000 to Grafana's port 3000 so that we can access the Grafana dashboard through localhost:3000
- `port-forward-prometheus.sh:` Bash script to port forward port 9090 to Prometheus' port 9090 so that we can access the Prometheus dashboard through localhost:9090
### prometheus-adapter:
- `helm-install.sh:` Bash script that installs the Prometheus operator using helm
- `helm-uninstall.sh:` Bash script that removes the Prometheus operator from the cluster
- `override.yaml:` Yaml of overrides of default values for the prometheus adapter to work correctly
- `README.md:` Markdown file explaining how to setup the Prometheus operator 
### src:
 - `metrics.py:` Simple python script that gathers Knative/Kubeless service metrics
