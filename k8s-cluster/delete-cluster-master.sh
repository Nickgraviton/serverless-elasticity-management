kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl drain sol --delete-local-data --force --ignore-daemonsets
kubectl delete node sol
kubectl drain luna --delete-local-data --force --ignore-daemonsets
kubectl delete node luna
sudo iptables -F
sudo iptables -t nat -F
sudo iptables -t mangle -F
sudo iptables -X
sudo kubeadm reset
sudo rm -rf ~/.kube/
sudo rm -rf /etc/cni/net.d
