# Setup metrics server
To download the yaml file run:
```bash
curl -LO https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```
Then add the following args to the yaml file:
```yaml
      - args:
        - --cert-dir=/tmp
        - --secure-port=4443
        - --kubelet-use-node-status-port
        - --kubelet-insecure-tls
        - --kubelet-preferred-address-types=InternalIP
```
