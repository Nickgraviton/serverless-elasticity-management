# Kubeless monitoring
In order to collect the metrics exposed by the kubeless runtimes, we can create a service monitor that selects services based on the label `created-by: kubeless` that is automatically added to services created by kubeless. Therefore we can run:
```bash
kubectl apply -f kubeless-servicemonitor.yaml
```
The yaml file is as follows:
```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: kubeless-servicemonitor
  namespace: monitoring
  labels:
    release: prometheus
spec:
  endpoints:
  - path: /metrics
    port: http-function-port
    interval: 3s
  namespaceSelector:
    matchNames:
    - default
  selector:
    matchLabels:
      created-by: kubeless
```
Now the Kubeless runtime metrics are available to Prometheus.
