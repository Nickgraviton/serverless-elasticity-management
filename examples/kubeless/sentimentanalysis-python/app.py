import json
import nltk
from textblob import TextBlob

def sentimentanalysis(event, context):
    nltk.download('punkt', download_dir='/tmp', quiet=True)

    text = event['data'].decode('utf-8')
    blob = TextBlob(text)
    res = {
        'polarity': 0,
        'subjectivity': 0
    }

    for sentence in blob.sentences:
        res['subjectivity'] = res['subjectivity'] + sentence.sentiment.subjectivity
        res['polarity'] = res['polarity'] + sentence.sentiment.polarity

    total = len(blob.sentences)

    res['sentence_count'] = total
    res['polarity'] = res['polarity'] / total
    res['subjectivity'] = res['subjectivity'] / total

    return(json.dumps(res))
