kubeless function deploy sentimentanalysis --runtime python3.7 \
                                           --from-file app.py \
                                           --handler app.sentimentanalysis \
                                           --env NLTK_DATA='/tmp' \
                                           --dependencies requirements.txt \
                                           --label service-name=sentimentanalysis \
                                           --memory 128Mi \
                                           --cpu 500m
