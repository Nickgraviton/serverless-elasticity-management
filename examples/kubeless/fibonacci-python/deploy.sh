kubeless function deploy fibonacci --runtime python3.7 \
                                   --from-file app.py \
                                   --handler app.fibonacci \
                                   --label service-name=fibonacci \
                                   --memory 128Mi \
                                   --cpu 500m
