def fibonacci(event, context):
    n = int(event['data'])

    a, b = 0, 1
    count = 0

    if n <= 0:
        return('Please enter a positive integer')
    elif n == 1:
        return(str(a))
    else:
        while count < n:
            a, b =  b, a + b
            count += 1
        return(str(a))
