kubeless function deploy hello --runtime python3.7 \
                                --from-file test.py \
                                --handler test.hello \
                                --label service-name=hello \
                                --memory 128Mi \
                                --cpu 500m
