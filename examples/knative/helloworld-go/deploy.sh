kn service create helloworld-go --image gcr.io/knative-samples/helloworld-go \
                                --env TARGET="Go Sample v1" \
                                --limit cpu=300m,memory=256Mi \
                                --label service-name=helloworld-go \
                                --concurrency-utilization 10
