# Sentiment analysis example
## How to build
If we are using minikube we need to first switch to minikube's docker daemon:
```bash
eval $(minikube docker-env)
```
To build the image we need to run:
```bash
docker build -t dev.local/sentimentanalysis-python .
```
The `dev.local` prefix is needed so that knative knows it's a local image.
## How to deploy
After the image has been created we simply need to run:
```bash
kubectl apply -f service.yaml
```
## How to call
Run:
```bash
curl -X POST -H "Content-Type: text/plain" --data @<path_to_file> http://knative-sentimentanalysis-python.default.127.0.0.1.nip.io:8000/
```
or:
```bash
curl -X POST -H "Content-Type: text/plain" --data '<Sentences>' http://knative-sentimentanalysis-python.default.127.0.0.1.nip.io:8000/
```
