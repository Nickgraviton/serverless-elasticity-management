import json
from flask import Flask, request
from textblob import TextBlob

app = Flask(__name__)

@app.route('/', methods=['POST'])
def sentimentanalysis():
    data = request.get_data().decode('utf-8')
    blob = TextBlob(data)
    res = {
        'polarity': 0,
        'subjectivity': 0
    }

    for sentence in blob.sentences:
        res['subjectivity'] = res['subjectivity'] + sentence.sentiment.subjectivity
        res['polarity'] = res['polarity'] + sentence.sentiment.polarity

    total = len(blob.sentences)

    res['sentence_count'] = total
    res['polarity'] = res['polarity'] / total
    res['subjectivity'] = res['subjectivity'] / total

    return(json.dumps(res))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
