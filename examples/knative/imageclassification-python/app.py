import os
import json
import numpy as np
from flask import Flask, request
from keras.applications.vgg16 import VGG16
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input,decode_predictions

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/tmp'

@app.route('/', methods=['POST'])
def image_classification():
    if 'file' in request.files:
        input_file = request.files['file']
        filename = input_file.filename
        img_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        input_file.save(img_path)

        model = VGG16(weights='imagenet')

        img = image.load_img(img_path, color_mode='rgb', target_size = (224,224))

        input_arr = image.img_to_array(img)
        input_arr = np.expand_dims(input_arr, axis=0)
        input_arr = preprocess_input(input_arr)

        features = model.predict(input_arr)
        predictions = decode_predictions(features)
        return(json.dumps(str(predictions)))
    else:
        return('Error: Request should have type "multipart/form-data" and an "image" field', 400)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
