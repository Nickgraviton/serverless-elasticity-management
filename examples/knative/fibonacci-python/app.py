from flask import Flask

app = Flask(__name__)

@app.route('/fib/<n>')
def fibonacci(n):
    n = int(n)

    a, b = 0, 1
    count = 0

    if n <= 0:
        return('Please enter a positive integer\n')
    elif n == 1:
        return(str(a) + '\n')
    else:
        while count < n:
            a, b =  b, a + b
            count += 1
        return(str(a) + '\n')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
