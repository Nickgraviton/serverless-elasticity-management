# Knative setup
After making Kourier a ClusterIP service we can redirect requests of the nginx ingress to the Kourier service by creating an ingress object for each application. For example:
```yaml
apiVersion: v1
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: <app_name>
  namespace: kourier-system
spec:
  rules:
  - host: "<app_name>.default.127.0.0.1.nip.io"
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: kourier
            port:
              number: 80
```
