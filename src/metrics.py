import sys
import requests
from kubernetes import client, config
from kubernetes.client.rest import ApiException

# Load kubernetes config
config.load_kube_config()

# Check if cpu string ends in m and convert to cores
def cpu_in_cores(cpu):
    if cpu.endswith('m'):
        return float(cpu.replace('m', '')) / 1000
    return float(cpu)

# Check if memory string ends in multiple of bytes and convert to bytes
def mem_in_bytes(mem):
    has_suffix = False
    units = {'K': 10**3, 'M': 10**6, 'G': 10**9, 'T': 10**12, 'P': 10**15, 'E': 10**18,
             'Ki': 2**10, 'Mi': 2**20, 'Gi': 2**30, 'Ti': 2**40, 'Pi': 2**50, 'Ei': 2**60}
    memory = 0
    for multiple in units:
        if mem.endswith(multiple):
            memory = float(mem.replace(multiple, '')) * units[multiple]
            has_suffix = True
    if has_suffix is False:
        memory = float(mem)
    return memory

def gather_pod_util_metrics(svc_name):
    # Select pods based on the service name label
    label_selector = 'service-name=' + svc_name

    # Bool flags to check if requests have been set
    cpu_req_enabled = True
    memory_req_enabled = True

    # Get all running/failed pods for app
    core = client.CoreV1Api()
    cpu_req_lookup = {}
    memory_req_lookup = {}
    try:
        pods = core.list_pod_for_all_namespaces(label_selector=label_selector)
        running_pods = 0
        failed_pods = 0
        for pod in pods.items:
            if pod.status.phase == 'Running':
                running_pods += 1
                pod_name = pod.metadata.name

                for container in pod.spec.containers:
                    # Kubeless names a container with the function name while Knative names the user app container as `user-container`
                    if container.name == svc_name or container.name == 'user-container':
                        try:
                            cpu_req_lookup[pod_name] = cpu_in_cores(container.resources.requests['cpu'])
                        except (TypeError, KeyError):
                            cpu_req_enabled = False

                        try:
                            memory_req_lookup[pod_name] = mem_in_bytes(container.resources.requests['memory'])
                        except (TypeError, KeyError):
                            memory_req_enabled = False

            elif pod.status.phase == 'Failed':
                failed_pods += 1

        print('Number of running pods for app:\t\t', running_pods)
        print('Number of failed pods for app:\t\t', failed_pods)
    except ApiException as error:
        print('Unable to fetch pods')
        print(error)

    # Get CPU/memory util
    custom = client.CustomObjectsApi()
    try:
        pods = custom.list_cluster_custom_object('metrics.k8s.io', 'v1beta1', 'pods', label_selector=label_selector)
        cpu_util = []
        memory_util = []
        for pod in pods['items']:
            for container in pod['containers']:
                if container['name'] == svc_name or container['name'] == 'user-container':
                    current_cpu = cpu_in_cores(container['usage']['cpu'])
                    current_memory = mem_in_bytes(container['usage']['memory'])

            pod_name = pod['metadata']['name']
            if cpu_req_enabled:
                cpu_request = cpu_req_lookup[pod_name]
                cpu_util.append(current_cpu / cpu_request)
            if memory_req_enabled:
                memory_request = memory_req_lookup[pod_name]
                memory_util.append(current_memory / memory_request)

        if cpu_req_enabled and len(cpu_util) != 0:
            print('Average CPU utilization for app:\t {:.2%}'.format(sum(cpu_util) / len(cpu_util)))
        else:
            if not cpu_req_enabled:
                print('CPU utilization cannot be reported because cpu request has not been set')
            else:
                print('CPU utilization cannot be reported because current cpu metrics cannot be fetched')

        if memory_req_enabled and len(memory_util) != 0:
            print('Average memory utilization for app:\t {:.2%}'.format(sum(memory_util) / len(memory_util)))
        else:
            if not memory_req_enabled:
                print('Memory utilization cannot be reported because memory request has not been set')
            else:    
                print('Memory utilization cannot be reported because current memory metrics cannot be fetched')
    except ApiException as error:
        print('Unable to fetch utilization metrics')
        print(error)

def gather_autoscaler_metrics(svc_name):
    label_selector = 'service-name=' + svc_name

    # Get HPA info
    auto_scaling = client.AutoscalingV2beta2Api()
    try:
        hpa_info = auto_scaling.list_horizontal_pod_autoscaler_for_all_namespaces(label_selector=label_selector)

        for hpa in hpa_info.items:
            for hpa_metric in hpa.spec.metrics:
                try:
                    print('HPA metric - target average util:\t', hpa_metric.resource.name,
                            '- {}%'.format(hpa_metric.resource.target_average_utilization))
                # If custom metric
                except AttributeError:
                    print('HPA metric - target average:\t\t', hpa_metric.pods.metric_name,
                            '- {}'.format(hpa_metric.pods.target_average_value))

            try:
                for current_metric in hpa.status.current_metrics:
                    try:
                        print('HPA metric - current value:\t\t', current_metric.resource.name,
                                '- {}%'.format(current_metric.resource.current_average_utilization))
                    except AttributeError:
                        print('HPA metric - current value:\t\t', current_metric.pods.metric_name,
                                '- {}'.format(current_metric.pods.current_average_value))
            except TypeError:
                print('Current metrics not yet available')
    except ApiException as error:
        print('Unable to fetch HPA info. Status code:\t', error.status)

    # Get KPA info for Knative
    try:
        label_selector = 'serving.knative.dev/service=' + svc_name
        custom = client.CustomObjectsApi()
        kpa_info = custom.list_cluster_custom_object('autoscaling.internal.knative.dev', 'v1alpha1', 'podautoscalers', label_selector=label_selector)

        for kpa in kpa_info['items']:
            try:
                print('KPA metric - target average util:\t', kpa['metadata']['annotations']['autoscaling.knative.dev/metric'],
                        '- {}%'.format(kpa['metadata']['annotations']['autoscaling.knative.dev/targetUtilizationPercentage']))
            except KeyError:
                try:
                    print('KPA metric - target average:\t\t', kpa['metadata']['annotations']['autoscaling.knative.dev/metric'],
                            '- {}'.format(kpa['metadata']['annotations']['autoscaling.knative.dev/target']))
                except KeyError:
                    print('KPA metric - target average:\t\t', kpa['metadata']['annotations']['autoscaling.knative.dev/metric'],
                            ' - target not explicitly set')
    except ApiException as error:
        print('Unable to fetch KPA info. Status code:\t', error.status)

def gather_latency_throughput_metrics(svc_name):
    try:
        # Get throughput
        response = requests.get('http://localhost:9090/api/v1/query?query='\
                'sum(rate(nginx_ingress_controller_request_duration_seconds_count{{ingress="{}", status="200"}}[2m])) by (ingress)'.format(svc_name))

        response_json = response.json()

        try:
            print('Throughput:\t\t\t\t {:.10f} successful-requests-per-second'.format(float(response_json['data']['result'][0]['value'][1])))
        except:
            print('Throughput unavailable')

        # Get latency
        response = requests.get('http://localhost:9090/api/v1/query?query='\
                '(sum(rate(nginx_ingress_controller_request_duration_seconds_sum{{ingress="{0}", status="200"}}[2m])) by (ingress))'\
                '/(sum(rate(nginx_ingress_controller_request_duration_seconds_count{{ingress="{0}", status="200"}}[2m])) by (ingress))'.format(svc_name))

        response_json = response.json()

        try:
            print('Latency:\t\t\t\t {:.10f} seconds'.format(float(response_json['data']['result'][0]['value'][1])))
        except:
            print('Latency unavailable')
    except requests.exceptions.ConnectionError:
        print('Prometheus unavailable')

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python metrics.py <name_of_service>')
        sys.exit(1)

    svc_name = sys.argv[1]

    print('{:=^90s}'.format(svc_name + ' stats'))

    gather_pod_util_metrics(svc_name)
    gather_autoscaler_metrics(svc_name)
    gather_latency_throughput_metrics(svc_name)

    print('=' * 90)
