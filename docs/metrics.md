# Metrics to collect per Microservice

- Pod current cpu utilization: refers to the average CPU utilization used by the set of active pods. Per pod, the CPU utilization = pod_current_cpu_in_cores/request_cpu_in_cores (or pod_current_cpu_in_cores/limit_cpu_in_cores ☑

- Pod current memory utilization: refers to the average memory utilization by the set of active pods. Per pod, the memory utilization = pod_current_memory_in_mb/request_memory_in_mb (or pod_current_memory_in_mb/limit_memory_in_mb) ☑

- Current number of deployed pods: refers to the number of deployed pods ☑

- Current number of pods terminated (backoff): refers to the number of terminated pods ☑

- Pod throughput: refers to the average throughput by the set of deployed pods in requests/time_period ☑

- Pod latency: refers to the average latency by the set of deployed pods in seconds ☑

- Check for KPA also by inspecting services ☑

- ExistsAnyHPA: this metric should be true if any HPA is enabled for the microservice ☑

- HPAMetric(s): The metric(s) the HPA is enabled for ☑

- HPAMetric(s) Thresholds: The thresholds of the HPA metric(s) ☑
