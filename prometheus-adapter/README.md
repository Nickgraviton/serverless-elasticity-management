# Setup prometheus adapter
We override some of the default values of the chart in `override.yaml`:
- The correct prometheus url is `http://prometheus-operated.monitoring.svc`
- We can also set the metric relist interval to 30 seconds
- Lastly, we create a rule that exposes a custom metric called `function_calls_per_second` that calculates the function calls per second for kubeless functions

By default the prometheus adapter config creates custom metrics based on metrics whose name ends in `total`. For example the python runtime exposes the `function_calls_total` metric and therefore the prometheus adapter makes a custom metric called `function_calls` available. However that metric calculates the 5 minute rate of the function calls and therefore its value changes slowly over time. To address this we can create our own metric by running:
```bash
KUBE_EDITOR=vim kubectl edit cm prometheus-adapter -n monitoring
```
and appending the following to the rules to get the `function_calls_per_second` metric:
```yaml
    - seriesQuery: 'function_duration_seconds_count{namespace!="",pod!=""}'
      resources:
        overrides:
          namespace:
            resource: namespace
          pod:
            resource: pod
      name:
        as: "function_calls_per_second"
      metricsQuery: sum(rate(<<.Series>>{<<.LabelMatchers>>}[30s])) by (<<.GroupBy>>)
```
